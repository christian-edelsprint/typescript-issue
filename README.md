# Instructions

1. Run `npm install`.
2. Run `forge deploy`.

The file `static/index.ts` contains a type error that prevents the app from being deployed:

```
ℹ Packaging app files

Error: TypeScript errors in the app caused the bundling to fail. Fix the errors listed below before rerunning the command. 
[tsl] ERROR in /[...]/static/index.ts(1,7)
      TS2322: Type '"123"' is not assignable to type 'number'.
```

However, files outside the `src` directory are used in a separate build process (e.g., `create-react-app`), so **`forge deploy` should ignore them and only compile code in the `src` directory**.

Since Forge tries to bundle the code with its own TypeScript configuration, this can also prevent deployments if a different configuration (`tsconfig.json`) is used in the `static` directory.
